/*
 * spaunge.cpp
 *
 *  Created on: Feb 19, 2016
 *      Author: wegmannd
 */

#include <sys/time.h>
#include "TSegDistort_core.h"

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" segDistort 0.1 ");
	logfile.write("****************");
    try{
		//read parameters from the command line
    	TParameters myParameters(argc, argv, &logfile);

		//verbose?
		bool verbose=myParameters.parameterExists("verbose");
		if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");
		logfile.setVerbose(verbose);


		//open log file that handles the output
		std::string  logFilename=myParameters.getParameterString("logFile", false);
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" segDistort 0.1 ");
			logfile.writeFileOnly("****************");
		}

		//initialize random generator
		logfile.listFlush("Initializing random generator ...");
		TRandomGenerator* randomGenerator;
		if(myParameters.parameterExists("fixedSeed")){
			randomGenerator = new TRandomGenerator(myParameters.getParameterLong("fixedSeed"), true);
		} else if(myParameters.parameterExists("addToSeed")){
			randomGenerator = new TRandomGenerator(myParameters.getParameterLong("addToSeed"), false);
		} else randomGenerator = new TRandomGenerator();
		logfile.write(" done with seed " + toString(randomGenerator->usedSeed) + "!");

		//create core object
		TSegDistort_core core(&logfile, randomGenerator);

		//what to do?
		std::string task = myParameters.getParameterStringWithDefault("task", "infer");
		if(task == "infer"){
			logfile.startIndent("Inferring distortions:");
			core.infer(myParameters);
		} else if(task == "simulate"){
			logfile.startIndent("Simulating data:");
			core.simulateData(myParameters);
		} else throw "Unknown task '" + task + "'!";
		logfile.clearIndent();

		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!=""){
			logfile.newLine();
			logfile.warning("The following parameters were not used: " + unusedParams + "!");
		}

		delete randomGenerator;
    }
	catch (std::string & error){
		logfile.error(error);
	}
	catch (const char* error){
		logfile.error(error);
	}
	catch(std::exception & error){
		logfile.error(error.what());
	}
	catch (...){
		logfile.error("unhandeled error!");
	}
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();
    return 0;
}






