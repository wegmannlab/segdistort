/*
 * TSegregationProbabilities.h
 *
 *  Created on: Jun 15, 2018
 *      Author: wegmannd
 */

#ifndef TSEGREGATIONPROBABILITIES_H_
#define TSEGREGATIONPROBABILITIES_H_

#include <iostream>
#include "TRandomGenerator.h"

//-----------------------------------------------------
//TSegregationProbabilities
//-----------------------------------------------------
class TSegregationProbabilities{
protected:
	int numStates;
	bool storageInitialized;

	void initializeStorage(int NumStates);
	void freeStorage();

	inline int getIndex(short & mother, short & father, int & distortionIndex){
		return distortionIndex*9 + mother*3 + father;
	};

	virtual void fillTransmissionMatrixMother(double** mother, double & distortionStrengh);
	virtual void fillTransmissionMatrixFather(double** father, double & distortionStrengh);

	void fillSegregationProbabilities(double* distortionStrengths, double Mu);
	void fillSegregationProbabilities(double** segregationProbabilities, double* distortionStrength, double Mu);

	void initCumulativeStorage();
	void freeCumulativeStorage();

public:
	//TODO: maybe move to protected?
	int sizeOfEmissionStorage;
	double** segProb;
	double** segProbMinus;

	double** segProbCumul;
	double** segProbMinusCumul;
	bool cumulativeStorageInitialized;

	TSegregationProbabilities(int NumStates);
	TSegregationProbabilities(int NumStates, double* distortionStrength, double mu);

	virtual ~TSegregationProbabilities(){
		freeStorage();
	};

	void fillCumulative();
	int sampleOffspringGenotype(short & mother, short & father, int & distortionIndex, TRandomGenerator & randomGenerator);
	int sampleOffspringGenotypeMinus(short & mother, short & father, int & distortionIndex, TRandomGenerator & randomGenerator);
	void printSegProbs();

};

class TSegregationProbabilitiesBackcross:public TSegregationProbabilities{
protected:
	void fillTransmissionMatrixFather(double** mother, double & distortionStrenghs);

	void fillSegregationProbabilities_OLD(double* distortionStrength, double Mu);

public:
	TSegregationProbabilitiesBackcross(int NumStates):TSegregationProbabilities(NumStates){};
	TSegregationProbabilitiesBackcross(int NumStates, double* distortionStrengths, double Mu);
	~TSegregationProbabilitiesBackcross(){};
};




#endif /* TSEGREGATIONPROBABILITIES_H_ */
