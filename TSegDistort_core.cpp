/*
 * TSegDistort_core.cpp
 *
 *  Created on: Apr 13, 2017
 *      Author: phaentu
 */


#include "TSegDistort_core.h"
//-----------------------------------------------------
//TSites
//-----------------------------------------------------
void TSites::fill(std::vector<long> & Distances, long & maxDist){
	if(initialized)
		throw "Distance groups were already initialized!";

	//create distance groups
	numDistanceGroups = ceil(log(maxDist)/log(2)) + 1; //one extra for beginning of chromosome
	distanceGroups = new TDistanceGroup[numDistanceGroups];
	groupHasSites = new bool[numDistanceGroups];
	for(int d=1; d<numDistanceGroups;d++){
		groupHasSites[d]=false;
	}
	numSites = Distances.size();
	groupMembership = new int[numSites]; //artificial Q at end
	siteIsBeginningOfChromosome = new bool[numSites];
	initialized = true;

	//group 0: first sites of chromosomes
	distanceGroups[0].min = -1;
	distanceGroups[0].maxPlusOne = -1;
	distanceGroups[0].distance = -1.0;

	//all other groups
	distanceGroups[1].min = 1;
	int g;
	for(g=2; g<numDistanceGroups; ++g){
		distanceGroups[g].min = distanceGroups[g-1].min * 2;
		distanceGroups[g-1].maxPlusOne = distanceGroups[g].min;
		distanceGroups[g-1].distance = distanceGroups[g-1].maxPlusOne - distanceGroups[g-1].min;
	}
	distanceGroups[numDistanceGroups - 1].maxPlusOne = distanceGroups[numDistanceGroups-1].min * 2.0 + 1.0;

	//assign sites to groups
	int l = 0;
	int max = numDistanceGroups - 1;

	for(std::vector<long>::iterator it = Distances.begin(); it != Distances.end(); ++it, ++l){
		if(*it < 0){ //means is at beginning of chromosome
			siteIsBeginningOfChromosome[l] = true;
			groupMembership[l] = 0;
			groupHasSites[0] = true;
		} else {
			for(g=1; g<numDistanceGroups; ++g){
				if(g == max){
					groupMembership[l] = g;
					break;
				} else if(*it < distanceGroups[g].maxPlusOne){
					groupMembership[l] = g;
					break;
				}
			}
			groupHasSites[g] = true;
			siteIsBeginningOfChromosome[l] = false;
		}
	}
};

void TSites::fillTransitionMatrix(TSquareMatrixStorage & matrix, const double & kappa, const double distance){
	if(numStates < 0)
		throw "Can not fill transition matrix: numStates has not been set!";

	//create lambda matrix
	TBandMatrixStorage lambda(numStates,3);
	lambda.set(0.0);
	double kappaDist = kappa * distance;
	double minusTwoKappaDist = -2.0 * kappaDist;
	lambda(0,0) = -kappaDist; lambda(0,1) = kappaDist;
	for(int k=1; k<(numStates-1); k++){
		lambda(k,k-1) = kappaDist; lambda(k,k) = minusTwoKappaDist; lambda(k,k+1) = kappaDist;
	}
	lambda(numStates-1,numStates-1) = -kappaDist; lambda(numStates-1,numStates-2) = kappaDist;

	//now get exponential of lambda
	matrix.fillAsExponential(lambda);
};

void TSites::fillAllTransitionProbabilityMatrices(const double & kappa){
	if(numStates < 0)
		throw "Can not fill transition matrix: numStates has not been set!";

	if(!Q_initialized){
		Q = new TSquareMatrixStorage[numDistanceGroups];
		Q_initialized = true;
		Q[0].resize(numStates);
		Q[0].set((1.0/(double)numStates));
	}

	//get Q[1] for distance 1
	fillTransitionMatrix(Q[1], kappa, 1.0);

	//then fill all other Q
	for(int k=2; k<numDistanceGroups; ++k){
		Q[k].fillFromSquare(Q[k-1]);
	}
};

double& TSites::transProb(const long & locus, const int & from, const int & to){
	return Q[groupMembership[locus]](from, to);
};


void TSites::setGamma_ij_zero(){
	if(numStates < 0)
		throw "Can not fill transition matrix: numStates has not been set!";

	//initialize if not yet done
	if(!gamma_ij_initalized){
		gamma_ij = new double**[numDistanceGroups];
		for(int d=0; d<numDistanceGroups; ++d){
			gamma_ij[d] = new double*[numStates];
			for(int i=0; i<numStates; ++i){
				gamma_ij[d][i] = new double[numStates];
			}
		}
		gamma_ij_initalized = true;
	}

	//set zero
	for(int d=0; d<numDistanceGroups; ++d){
		for(int i=0; i<numStates; ++i){
			for(int j=0; j<numStates; ++j){
				gamma_ij[d][i][j] = 0.0;
			}
		}
	}
};

void TSites::addToGamma_ij(double val, const long & locus, const int & from, const int & to){
	gamma_ij[groupMembership[locus]][from][to] += val;
};

void TSites::updateInitialFrequency(double* aj){
	double** data = Q[0].data;
	for(int i=0; i<numStates; ++i){
		for(int j=0; j<numStates; ++j){
			data[i][j] = aj[j];
		}
	}
};

double TSites::estimateKappaQ2(){
	double Q2 = 0.0;
	for(int d=1; d<numDistanceGroups; ++d){
		if(groupHasSites[d]){
			for(int i=0; i<numStates; ++i){
				for(int j=0; j<numStates; ++j){
					Q2 += gamma_ij[d][i][j] * log(Q[d](i,j));
				}
			}
		}
	}
	return Q2;
};

double TSites::getNewKappaEstimate(double curKappa, int NumSwitches){
	//maximize kappa using a simple algorithm
	double oldQ2;
	double logStep = 1.0;
	double curQ2 = estimateKappaQ2();

	//now make steps
	int numSwitches = 0;
	int it;
	for(it=0; it<1000; ++it){
		//update kappa
		curKappa = exp(log(curKappa) + logStep);

		//get new Q2
		oldQ2 = curQ2;
		fillAllTransitionProbabilityMatrices(curKappa);
		curQ2 = estimateKappaQ2();

		//do we change direction?
		if(curQ2 < oldQ2){
			logStep = -logStep / 2.0;
			++numSwitches;

			//do we stop?
			if(numSwitches == NumSwitches)
				break;
		}
	}
	if(it >= 1000)
		throw "Kappa optimization did not converge! Use different starting value (last estimate: " + toString(curKappa) + ").";

	return curKappa;
};

void TSites::calcKappQ2Surface(std::string filename){
	//open file
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open file " + filename + " for writing!";

	//header
	out << "log(kappa)\tkappa\tQ2\n";

	//now calc and print Q2 surface (on log scale)
	int size = 3001;
	double min = -5.0;
	double max = -2.0;
	double step = (max - min) / (double) (size-1);
	double kappa;
	double logKappa;

	for(int i=0; i<size; ++i){
		logKappa = min + i*step;
		kappa = pow(10.0, logKappa);
		fillAllTransitionProbabilityMatrices(kappa);
		out << logKappa << "\t" << kappa << "\t" << estimateKappaQ2() << "\n";
	}
	out.close();
};

//-----------------------------------------------------
//TSegDistort_core
//-----------------------------------------------------
TSegDistort_core::TSegDistort_core(TLog* Logfile, TRandomGenerator* RandomGenerator){
	logfile = Logfile;
	randomGenerator = RandomGenerator;
	initialized = false;
	segProbs = NULL;
	segProbInitialized = false;
	distortionStrength = NULL;
	numStates = 0;
	emissionProbabilities = NULL;
	emissionProbabilitiesInitialized = false;

	//HMM
	alpha = NULL;
	initialFreq_aj = NULL;
	beta = NULL;
	betaNext = NULL;
	gamma_i = NULL;
	gamma_ij = NULL;
	hmmVariablesInitialized = false;
}

void TSegDistort_core::clear(){
	if(initialized){
		delete[] distortionStrength;
		initialized = false;
	}

	if(segProbInitialized)
		delete segProbs;

	if(hmmVariablesInitialized){
		for(long l=0; l<sites.numSites; ++l)
			delete[] alpha[l];
		delete[] alpha;
		delete[] initialFreq_aj;
		delete[] beta;
		delete[] betaNext;
		delete[] gamma_i;
		for(int s=0; s<numStates; ++s)
			delete[] gamma_ij[s];
		delete[] gamma_ij;
		hmmVariablesInitialized = false;
	}

	if(emissionProbabilitiesInitialized){
		for(long l=0; l<sites.numSites; ++l)
			delete[] emissionProbabilities[l];
		delete[] emissionProbabilities;
	}
}

void TSegDistort_core::initialize(int NumStates){
	if(initialized)
		throw "States were already initialized!";

	numStates = NumStates;
	logfile->list("Will use a transition matrix with " + toString(numStates) + " states.");
	if(numStates < 2)
		throw "Need at least 2 states!";

	//fill distortion strength
	distortionStrength = new double[numStates];
	for(int i=0; i<numStates; ++i)
		distortionStrength[i] = 0.5 * (double) i / (double) (numStates - 1);

	initialized = true;
}

void TSegDistort_core::createHMMstorage(){
	logfile->listFlush("Initializing HMM storage ...");
	if(hmmVariablesInitialized)
		throw "HMM storage was already initialized!";

	alpha = new double*[sites.numSites];
	for(long l=0; l<sites.numSites; ++l)
		alpha[l] = new double[numStates];

	beta = new double[numStates];
	betaNext = new double[numStates];
	initialFreq_aj = new double[numStates];
	gamma_i = new double[numStates];
	gamma_ij = new double*[numStates];
	for(int s=0; s<numStates; ++s)
		gamma_ij[s] = new double[numStates];

	hmmVariablesInitialized = true;
	logfile->done();
}

void TSegDistort_core::printProgressReadData(long lines, long & snps, int chromosomes, struct timeval & start){
	struct timeval end;
	gettimeofday(&end, NULL);
	float runtime = (end.tv_sec  - start.tv_sec)/60.0;
	logfile->list("Parsed " + toString(lines) + " lines and added " + toString(snps) + " variants on " + toString(chromosomes) + " chromosomes in " + toString(runtime) + " min");
};

void TSegDistort_core::readData(std::string filename, std::string motherName, std::string fatherName, long maxNumSites, bool requireParentsHaveData, bool requireOneParentHasData, double missingness){
	//open VCF file depending on ending
	std::string ending = readAfterLast(filename, '.');
	if(ending == "gz"){
		logfile->list("Opening zipped VCF from file '" + filename + "'");
		vcfFile.openStream(filename, true);
	} else {
		logfile->list("Opening VCF from file '" + filename + "'");
		vcfFile.openStream(filename, false);
	}
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	logfile->conclude("Found data for " + toString(vcfFile.numSamples()) + " individuals.");

	//find mother and father
	logfile->listFlush("Looking for mother '" + motherName + "' and father '" + fatherName + "' ...");
	int motherId = vcfFile.sampleNumber(motherName);
	int fatherId = vcfFile.sampleNumber(fatherName);
	logfile->done();
	int numOffspring = vcfFile.numSamples()-2;
	int* otherId = new int[numOffspring];
	int s, i=0;
	for(int s=0; s<vcfFile.numSamples(); ++s){
		if(s!=motherId && s!=fatherId){
			otherId[i] = s;
			++i;
		}
	}

	//prepare temporary storage
	std::vector<double*> emissionProbabilities_offspring;
	std::vector<double*> genotypeLikelihoodsMother;
	std::vector<double*> genotypeLikelihoodsFather;
	int MLE_geno;
	double* genoFreqMother = new double[3];
	double* genoFreqFather = new double[3];
	genoFreqMother[0] = 0; genoFreqMother[1] = 0; genoFreqMother[2] = 0;
	genoFreqFather[0] = 0; genoFreqFather[1] = 0; genoFreqFather[2] = 0;
	int sizeOfEmissionStorage = segProbs->sizeOfEmissionStorage;
	double* emissionStorageAlpha = new double[sizeOfEmissionStorage];
	double* emissionStorageOneMinusAlpha = new double[sizeOfEmissionStorage];
	double* genotypeLikelihoods = new double[3];
	double* pointer;
	distances.clear();
	chromosomes.clear();
	long maxDist = 0;

	//parse VCF file and calculate emission probabilities
	if(maxNumSites > 0)
		logfile->startIndent("Parsing through VCF file up to " + toString(maxNumSites) + " sites:");
	else
		logfile->startIndent("Parsing through VCF file:");
	long numSnpsParsed = 0;
	long progressFrequency = 10000;
	double alpha = 0.5; //TODO: use alpha more sensibly
	struct timeval start;
	gettimeofday(&start, NULL);
	int oldPos = 0;
	std::string oldChr = "";
	double max;
	int numParentMissing;

	//parse...
	while(vcfFile.next()){

		//only look at positions in VCF with two alleles that are not 'N'
		if(vcfFile.getNumAlleles() == 2 || (vcfFile.getNumAlleles() == 3 && vcfFile.getAllele(2) == "<NON_REF>")){

			//Keep only sites where parents have data
			numParentMissing = vcfFile.sampleIsMissing(motherId) + vcfFile.sampleIsMissing(fatherId);
			if(numParentMissing == 0 || (numParentMissing == 1 && !requireParentsHaveData) || (numParentMissing == 2 && !requireParentsHaveData && !requireOneParentHasData)){

				//keep only sites that match missingness
				if(vcfFile.missingness() <= missingness){

					//site is accepted!
					if(vcfFile.chr() != oldChr){
						chromosomes.push_back(vcfFile.chr());
						distances.push_back(-1); //-1 implies a new chromosome!
						oldChr = vcfFile.chr();
						initialPos.push_back(vcfFile.position());
						oldPos = 0;
					} else
						distances.push_back(vcfFile.position() - oldPos);

					//check if file is solted
					if(vcfFile.position() < oldPos)
						throw "VCF file is not sorted by position!";

					//read new pos and set max distance
					oldPos = vcfFile.position();
					if(*distances.rbegin() > maxDist)
						maxDist = *distances.rbegin();

					//store genotype likelihoods of parents and add MLE to counts
					//mother
					pointer = new double[3];
					genotypeLikelihoodsMother.push_back(pointer);
					vcfFile.fillGenotypeLikelihoods(motherId, pointer);

					MLE_geno = 0;
					if(pointer[1] > pointer[MLE_geno])
						MLE_geno = 1;
					if(pointer[2] > pointer[MLE_geno])
						MLE_geno = 2;
					++genoFreqMother[MLE_geno];

					//father
					genotypeLikelihoodsFather.push_back(new double[3]);
					pointer = *genotypeLikelihoodsFather.rbegin();
					vcfFile.fillGenotypeLikelihoods(fatherId, pointer);

					MLE_geno = 0;
					if(pointer[1] > pointer[MLE_geno])
						MLE_geno = 1;
					if(pointer[2] > pointer[MLE_geno])
						MLE_geno = 2;
					++genoFreqFather[MLE_geno];


					//parse genotype likelihoods of offspring and calc emission probabilities
					/*
					for(i=0; i<sizeOfEmissionStorage; ++i){
						emissionStorageAlpha[i] = 1.0;
						emissionStorageOneMinusAlpha[i] = 1.0;
					}
					for(s=0; s<numOffspring; ++s){
						vcfFile.fillGenotypeLikelihoods(otherId[s], genotypeLikelihoods);
						for(i=0; i<sizeOfEmissionStorage; ++i){
							emissionStorageAlpha[i] *= genotypeLikelihoods[0]*segProb[i][0] + genotypeLikelihoods[1]*segProb[i][1] + genotypeLikelihoods[2]*segProb[i][2];
							emissionStorageOneMinusAlpha[i] *= genotypeLikelihoods[0]*segProbMinus[i][0] + genotypeLikelihoods[1]*segProbMinus[i][1] + genotypeLikelihoods[2]*segProbMinus[i][2];
						}
					}
					*/

					//alternative using log
					for(i=0; i<sizeOfEmissionStorage; ++i){
						emissionStorageAlpha[i] = 0.0;
						emissionStorageOneMinusAlpha[i] = 0.0;
					}
					for(s=0; s<numOffspring; ++s){
						vcfFile.fillGenotypeLikelihoods(otherId[s], genotypeLikelihoods);
						for(i=0; i<sizeOfEmissionStorage; ++i){
							emissionStorageAlpha[i] += log(genotypeLikelihoods[0]*segProbs->segProb[i][0] + genotypeLikelihoods[1]*segProbs->segProb[i][1] + genotypeLikelihoods[2]*segProbs->segProb[i][2]);
							emissionStorageOneMinusAlpha[i] += log(genotypeLikelihoods[0]*segProbs->segProbMinus[i][0] + genotypeLikelihoods[1]*segProbs->segProbMinus[i][1] + genotypeLikelihoods[2]*segProbs->segProbMinus[i][2]);
						}
					}
					//normalize
					max = emissionStorageAlpha[0];
					for(i=0; i<sizeOfEmissionStorage; ++i){
						if(emissionStorageAlpha[i]>max) max = emissionStorageAlpha[i];
						if(emissionStorageOneMinusAlpha[i]>max) max = emissionStorageOneMinusAlpha[i];
					}
					for(i=0; i<sizeOfEmissionStorage; ++i){
						emissionStorageAlpha[i] = exp(emissionStorageAlpha[i]-max);
						emissionStorageOneMinusAlpha[i] = exp(emissionStorageOneMinusAlpha[i]-max);
					}

					emissionProbabilities_offspring.push_back(new double[sizeOfEmissionStorage]);
					pointer = *emissionProbabilities_offspring.rbegin();
					for(i=0; i<sizeOfEmissionStorage; ++i){
						pointer[i] = alpha * emissionStorageAlpha[i] + (1.0 - alpha) * emissionStorageOneMinusAlpha[i];
					}

					//add to SNPs parsed
					++numSnpsParsed;
				}//end if site has not too much missing data
			} //end if parents have data
		} //end if site is OK

		//report progress
		if(vcfFile.currentLine % progressFrequency == 0){
			printProgressReadData(vcfFile.currentLine, numSnpsParsed, chromosomes.size(), start);
		}

		//check if we reach limit of sites to parse
		if(maxNumSites > 0 && numSnpsParsed > maxNumSites)
			break;
	}
	//report progress
	printProgressReadData(vcfFile.currentLine, numSnpsParsed, chromosomes.size(), start);
	logfile->endIndent();

	//assign distance groups
	logfile->listFlush("Assigning sites to distance groups ...");
	sites.fill(distances, maxDist);
	logfile->done();

	//estimate genotype frequencies
	logfile->listFlush("Estimating genome-wide genotype probabilities ...");
	double sum = genoFreqMother[0] + genoFreqMother[1] + genoFreqMother[2];
	genoFreqMother[0] /= sum; genoFreqMother[1] /= sum; genoFreqMother[2] /= sum;
	sum = genoFreqFather[0] + genoFreqFather[1] + genoFreqFather[2];
	genoFreqFather[0] /= sum; genoFreqFather[1] /= sum; genoFreqFather[2] /= sum;
	logfile->done();
	logfile->conclude("For the mother: " + toString(genoFreqMother[0]) + ", " +  toString(genoFreqMother[1]) + " and " + toString(genoFreqMother[2]) + ".");
	logfile->conclude("For the father: " + toString(genoFreqFather[0]) + ", " +  toString(genoFreqFather[1]) + " and " + toString(genoFreqFather[2]) + ".");


	//fill emission probabilities
	logfile->listFlush("Calculating emission probabilities ...");
	emissionProbabilities = new double*[sites.numSites];
	long l = 0;
	int g, index, index_state;
	double tmp;
	std::vector<double*>::iterator mIt = genotypeLikelihoodsMother.begin();
	std::vector<double*>::iterator fIt = genotypeLikelihoodsFather.begin();
	for(std::vector<double*>::iterator it = emissionProbabilities_offspring.begin(); it != emissionProbabilities_offspring.end(); ++it, ++l, ++mIt, ++fIt){
		emissionProbabilities[l] = new double[numStates];
		for(i=0; i<numStates; ++i){
			index_state = i*9;
			emissionProbabilities[l][i] = 0.0;
			for(g=0; g<3; ++g){
				index = 3*g + index_state;
				tmp = (*fIt)[0] * genoFreqFather[0] * (*it)[index] + (*fIt)[1] * genoFreqFather[1] * (*it)[index+1] + (*fIt)[2] * genoFreqFather[2] * (*it)[index+2];
				emissionProbabilities[l][i] += (*mIt)[g] * genoFreqMother[g] * tmp;
			}
		}
	}
	emissionProbabilitiesInitialized = true;
	logfile->done();

	//clean up
	delete[] otherId;
	delete[] emissionStorageAlpha;
	delete[] emissionStorageOneMinusAlpha;
	delete[] genotypeLikelihoods;
	delete[] genoFreqMother;
	delete[] genoFreqFather;
	for(std::vector<double*>::iterator it = emissionProbabilities_offspring.begin(); it != emissionProbabilities_offspring.end(); ++it)
		delete[] *it;
	for(std::vector<double*>::iterator it = genotypeLikelihoodsMother.begin(); it != genotypeLikelihoodsMother.end(); ++it)
		delete[] *it;
	for(std::vector<double*>::iterator it = genotypeLikelihoodsFather.begin(); it != genotypeLikelihoodsFather.end(); ++it)
		delete[] *it;
};

void TSegDistort_core::writeEmissionprobabilities(std::string & outname){
	std::string filename = outname + "_emissionProbabilities.txt.gz";
	logfile->listFlush("Writing emission probabilities to '" + filename + "' ...");

	//open file
	gz::ogzstream out(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";

	//prepare variables
	distIt = distances.begin();
	chrIt = chromosomes.end();
	initialPosIt = initialPos.end();
	long pos = 0;
	int i;

	//loop over sites
	for(long l=0; l<sites.numSites ;++l){
		//chromosome and position
		if(*distIt < 0){ //new chromosome
			if(chrIt == chromosomes.end()){
				chrIt = chromosomes.begin();
				initialPosIt = initialPos.begin();
			} else {
				++chrIt;
				++initialPosIt;
				if(chrIt == chromosomes.end())
					throw "Reached end of chromosome list while printing posterior probabilities!";
			}
			pos = *initialPosIt;
		} else
			pos += *distIt;
		out << *chrIt << "\t" << pos;

		for(i=0; i<numStates; ++i)
			out << "\t" << emissionProbabilities[l][i];
		out << "\n";
	}
	logfile->done();
	out.close();
}

double TSegDistort_core::runForward(){
	if(!hmmVariablesInitialized)
		throw "Can not run HMM: variables not yet initialized!";

	int s, s_old;
	logfile->listFlush("Running forward ...");

	//first for locus l=0
	double normSum = 0.0;
	int zero = 0;
	for(s=0; s<numStates; ++s){
		alpha[0][s] = sites.transProb(zero, zero, s) * emissionProbabilities[0][s];
		normSum += alpha[0][s];
	}

	for(s=0; s<numStates; ++s)
		alpha[0][s] /= normSum;
	double LL = log(normSum);

	//now for all other loci
	long old_l;
	for(long l=1; l<sites.numSites; ++l){
		old_l = l-1;
		normSum = 0.0;

		//calculate for each state
		for(s=0; s<numStates; ++s){
			alpha[l][s] = 0.0;
			for(s_old=0; s_old<numStates; ++s_old){
				alpha[l][s] += alpha[old_l][s_old] * sites.transProb(l, s_old, s);
			}
			alpha[l][s] *= emissionProbabilities[l][s];
			normSum += alpha[l][s];
		}

		//normalize and add to LL
		for(s=0; s<numStates; ++s)
			alpha[l][s] /= normSum;
		LL += log(normSum);
	}
	logfile->done();
	logfile->conclude("log-likelihood was " + toString(LL));
	return LL;
}

void TSegDistort_core::runBackwardAndEstimate(double curKappa, int maxNumIterationsKappa, int iteration, bool estimateKappa){
	if(!hmmVariablesInitialized)
		throw "Can not run HMM: variables not yet initialized!";

	//also estimate initial frequencies, so set them to zero.
	logfile->listFlush("Running backward ...");
	int s, s_old;
	double* pointer;
	sites.setGamma_ij_zero();
	for(s=0; s<numStates; ++s)
		initialFreq_aj[s] = 0.0;

	//fill for last locus
	double normSum = 0.0;
	double tmp = 1.0 / numStates;
	for(s=0; s<numStates; ++s){
		beta[s] = tmp;
	}

	//all other loci except first
	long next;
	for(long l=sites.numSites - 2; l>=0; --l){
		//swap betas
		pointer = beta; beta = betaNext; betaNext = pointer;

		//calc beta
		next = l+1;
		normSum = 0.0;
		for(s_old=0; s_old<numStates; ++s_old){
			beta[s_old] = 0.0;
			for(s=0; s<numStates; ++s){
				beta[s_old] += betaNext[s] * sites.transProb(next, s_old, s) * emissionProbabilities[next][s];
			}
			normSum += beta[s_old];
		}

		//normalize
		for(s=0; s<numStates; ++s)
			beta[s] /= normSum;

		//Add to gamma_ij used to infer kappa, if next is not first of chromosome
		if(!sites.siteIsBeginningOfChromosome[next]){
			normSum = 0.0;
			//calculate gamma_ij
			for(s_old=0; s_old<numStates; ++s_old){
				for(s=0; s<numStates; ++s){
					gamma_ij[s_old][s] = alpha[l][s_old] * betaNext[s] * sites.transProb(next, s_old, s) * emissionProbabilities[next][s];
					normSum += gamma_ij[s_old][s];
				}
			}
			//add to sites storage
			for(s_old=0; s_old<numStates; ++s_old){
				for(s=0; s<numStates; ++s){
					sites.addToGamma_ij(gamma_ij[s_old][s] / normSum, next, s_old, s);
				}
			}
		}

		//Add site to infer initial frequencies, if is first of chromosome
		if(sites.siteIsBeginningOfChromosome[l]){
			normSum = 0.0;
			for(s=0; s<numStates; ++s){
				gamma_i[s] = alpha[l][s] * beta[s];
				normSum += gamma_i[s];
			}
			for(s=0; s<numStates; ++s){
				initialFreq_aj[s] += gamma_i[s] / normSum;
			}
		}
	}
	logfile->done();

	//normalize initial frequencies
	logfile->listFlush("Updating initial frequencies ...");
	normSum = 0.0;
	for(s=0; s<numStates; ++s)
		normSum += initialFreq_aj[s];
	for(s=0; s<numStates; ++s)
		initialFreq_aj[s] /= normSum;
	sites.updateInitialFrequency(initialFreq_aj);
	logfile->done();
	std::string newFreqString = "New frequencies are: " + toString(initialFreq_aj[0]);
	for(s=1; s<numStates; ++s)
		newFreqString += ", " + toString(initialFreq_aj[s]);
	newFreqString += ".";
	logfile->conclude(newFreqString);

	//update kappa
	//logfile->listFlush("Printing Kappa Q2 surface ...");
	//std::string filename = "kappaQ2Surface_" + toString(iteration) + ".txt";
	//sites.calcKappQ2Surface(filename);
	//logfile->done();

	if(estimateKappa){
		logfile->listFlush("Updating kappa ...");
		double kappa = sites.getNewKappaEstimate(curKappa, maxNumIterationsKappa);
		logfile->done();
		logfile->conclude("Estimated kappa at " + toString(kappa));
	}
}


void TSegDistort_core::runBackwardAndPrintPosteriorProbabilities(std::string filename){
	if(!hmmVariablesInitialized)
		throw "Can not run HMM: variables not yet initialized!";

	//need to store all gamma
	double** allGamma_i = new double*[sites.numSites];

	logfile->listFlush("Running backward and calculating posterior probabilities ...");
	int s, s_old;
	double* pointer;
	sites.setGamma_ij_zero();

	//fill for last locus
	double normSum = 0.0;
	double normSum2 = 0.0;
	double tmp = 1.0 / numStates;
	long l=sites.numSites-1;
	allGamma_i[l] = new double[numStates];
	for(s=0; s<numStates; ++s){
		beta[s] = tmp;
		allGamma_i[l][s] = alpha[l][s] * beta[s];
		normSum += allGamma_i[l][s];
	}
	for(s=0; s<numStates; ++s)
		allGamma_i[l][s] /= normSum;

	//all other loci except first
	long next;
	for(long l=sites.numSites - 2; l>=0; --l){
		//swap betas
		pointer = beta; beta = betaNext; betaNext = pointer;

		//calc beta
		next = l+1;
		normSum = 0.0;
		for(s_old=0; s_old<numStates; ++s_old){
			beta[s_old] = 0.0;
			for(s=0; s<numStates; ++s){
				beta[s_old] += betaNext[s] * sites.transProb(l, s_old, s) * emissionProbabilities[next][s];
			}
			normSum += beta[s_old];
		}

		//normalize and calc gamma_i
		allGamma_i[l] = new double[numStates];
		normSum2 = 0.0;
		for(s=0; s<numStates; ++s){
			beta[s] /= normSum;
			allGamma_i[l][s] = alpha[l][s] * beta[s];
			normSum2 += allGamma_i[l][s];
		}
		for(s=0; s<numStates; ++s)
			allGamma_i[l][s] /= normSum2;
	}
	logfile->done();

	//now print
	logfile->listFlush("Writing posterior probabilities to '" + filename + "' ...");
	gz::ogzstream out(filename.c_str());
	if(!out)
		throw "Failed to open file'" + filename + "' for writing!";

	//write header
	out << "chr\tpos";
	for(s=0; s<numStates; ++s)
		out << "\tP(pi=" << distortionStrength[s] << "|D)";
	out << "\n";

	//write posterior porbabilities
	distIt = distances.begin();
	chrIt = chromosomes.end();
	initialPosIt = initialPos.end();
	long pos = 0;

	for(l=0; l<sites.numSites; ++l){
		//chromosome and position
		if(*distIt < 0){ //new chromosome
			if(chrIt == chromosomes.end()){
				chrIt = chromosomes.begin();
				initialPosIt = initialPos.begin();
			} else {
				++chrIt;
				++initialPosIt;
				if(chrIt == chromosomes.end())
					throw "Reached end of chromosome list while printing posterior probabilities!";
			}
			pos = *initialPosIt;
		} else
			pos += *distIt;
		out << *chrIt << "\t" << pos;

		//now posterior probs
		for(s=0; s<numStates; ++s)
			out << "\t" << allGamma_i[l][s];
		out << "\n";

		//next distance
		++distIt;
	}

	//clean up
	out.close();
	for(l=0; l<sites.numSites; ++l)
		delete[] allGamma_i[l];
	delete[] allGamma_i;

	logfile->done();
};

void TSegDistort_core::initializeSegregationProbabilities(TParameters & params){
	//initialize segregation matrix
	if(segProbInitialized)
		delete segProbs;

	//read parameters
	std::string segModel = params.getParameterStringWithDefault("model", "randomCross");
	double mu = params.getParameterDoubleWithDefault("mu", 0.0);
	if(mu < 0.0 || mu > 0.5)
		throw "mu must be between 0.0 and 0.5!";

	logfile->listFlush("Initializing segregation model '" + segModel + "' with mu = " + toString(mu) + " ...");
	if(segModel == "randomCross")
		segProbs = new TSegregationProbabilities(numStates, distortionStrength, mu);
	else if(segModel == "backCross")
		segProbs = new TSegregationProbabilitiesBackcross(numStates, distortionStrength, mu);
	else
		throw "Unknown model '" + segModel + "'! Use either 'randomCross' or 'backCross'.";
	segProbInitialized = true;
	logfile->done();
};

void TSegDistort_core::infer(TParameters & params){
	//initialize
	logfile->startIndent("Initializing Baum-Welch algorithm:");

	std::string filename = params.getParameterString("vcf");
	logfile->list("Will use data from VCF file '" + filename + "'.");
	std::string motherName = params.getParameterString("mother");
	logfile->list("Will use individual '" + motherName + "' as mother.");
	std::string fatherName = params.getParameterString("father");
	logfile->list("Will use individual '" + fatherName + "' as father.");
	if(motherName == fatherName)
		throw "Father and Mother can not be the same individual!";

	long maxNumSites = -1;
	if(params.parameterExists("maxNumSites")){
		maxNumSites = params.getParameterLong("maxNumSites");
		if(maxNumSites < 10)
			throw "Need to read at leats 10 sites!";
	}

	bool requireParentsHaveData = params.parameterExists("requireBothParents");
	bool requireAtLeastOneParentHasData = false;
	if(requireParentsHaveData)
		logfile->list("Will only consider sites where both parents have data.");
	else {
		requireAtLeastOneParentHasData = params.parameterExists("requireOneParent");
		if(requireAtLeastOneParentHasData)
			logfile->list("Will only consider sites where at least one parent has data.");
	}

	double missingness = params.getParameterDoubleWithDefault("missingness", 1.0);
	if(missingness < 0.0 || missingness > 1.0)
		throw "Missingness must be between 0.0 and 1.0!";
	logfile->list("Will accept sites with missingness up to " + toString(missingness));

	initialize(params.getParameterIntWithDefault("numStates", 10));

	//segregation probabilites
	initializeSegregationProbabilities(params);
	segProbs->fillCumulative();

	//kappa
	bool estimateKappa = true;
	double kappa;
	if(params.parameterExists("kappa")){
		estimateKappa = false;
		kappa = params.getParameterDouble("kappa");
		logfile->list("Will set kappa fixed at " + toString(kappa) + ".");
	} else {
		kappa = params.getParameterDoubleWithDefault("initialKappa", 0.0001);
		logfile->list("Will estimate kappa using an initial kappa = " + toString(kappa) + ".");
	}

	double epsilonBW = params.getParameterDoubleWithDefault("epsilon", 0.001);
	logfile->list("Will run Baum-Welch until the log-likelihood difference < " + toString(epsilonBW) + ".");
	if(epsilonBW < 0.00000000001)
		throw "epsilonBW must be at least 0.00000000001!";

	int maxNumIterationsKappa = params.getParameterIntWithDefault("kappaIterations", 20);
	logfile->list("Will run kappa optimization for " + toString(maxNumIterationsKappa) + " iterations (sign switches).");
	if(maxNumIterationsKappa < 2)
			throw "Must optimize kappa for at least two iterations!";
	std::string outname = params.getParameterStringWithDefault("out", "");
	if(outname == ""){
		outname = filename;
		outname = extractBeforeLast(outname, '.');
		std::string ending = readAfterLast(filename, '.');
		if(ending == "gz")
			outname = extractBeforeLast(outname, '.');
	}
	logfile->list("Will write output files with prefix '" + outname + "'.");
	logfile->endIndent();

	//read data
	readData(filename, motherName, fatherName, maxNumSites, requireParentsHaveData, requireAtLeastOneParentHasData, missingness);

	//write emission probabilities
	if(params.parameterExists("writeEmissions")){
		writeEmissionprobabilities(outname);
	}

	//initialize transition matrices
	logfile->listFlush("Initializing transition matrices ...");
	sites.setNumStates(numStates);
	sites.fillAllTransitionProbabilityMatrices(kappa);
	logfile->done();

	//create HMM storage
	createHMMstorage();

	//run Baum Welch
	logfile->startIndent("Running Baum-Welch algorithm:");
	int iteration = 0;
	double LL = 0.0;
	double oldLL;
	double LLDiff;
	while(iteration < 1000){
		++iteration;
		logfile->startIndent("Baum-Welch iteration " + toString(iteration) + ":");
		oldLL = LL;

		//first run HMM
		LL = runForward();
		runBackwardAndEstimate(kappa, maxNumIterationsKappa, iteration, estimateKappa);

		//test for convergence
		if(iteration > 2){
			logfile->startIndent("Testing for convergence:");
			LLDiff = LL - oldLL;
			logfile->list("Difference in log-likelihood is " + toString(LLDiff));
			if(LLDiff < epsilonBW){
				logfile->conclude("Baum-Welch has converged!");
				logfile->endIndent();
				logfile->endIndent();
				break;
			} else {
				logfile->conclude("Baum-Welch has not yet converged.");
			}
			logfile->endIndent();
		}
		logfile->endIndent();
	}
	logfile->endIndent();


	//run HMM again to calculate posterior probabilities
	logfile->startIndent("running final HMM to get posterior probabilities:");
	LL = runForward();
	filename = outname + "_posteriorProbabilities.txt.gz";
	runBackwardAndPrintPosteriorProbabilities(filename);
	logfile->endIndent();
};

//-------------------------------------------
//Simulate data
//-------------------------------------------
void TSegDistort_core::simulateData(TParameters & params){
	//Reading basic parameters
	logfile->startIndent("Reading basic parameters:");
	int numOffspring = params.getParameterIntWithDefault("numOffspring", 100);
	logfile->list("Will simulate " + toString(numOffspring) + " offspring.");
	int numSites = params.getParameterIntWithDefault("numSites", 10000);
	int numChr = params.getParameterIntWithDefault("numChr", 2);
	int distance = params.getParameterIntWithDefault("distance", 1000);
	logfile->list("Will simulate " + toString(numChr) + " chromosomes, each with " + toString(numSites) + " sites " + toString(distance) + " bp apart.");
	float hetMother = params.getParameterDoubleWithDefault("hetM", 0.75);
	logfile->list("Will simulate mother's genotypes with heterozygosity " + toString(hetMother) + ".");
	float hetFather = params.getParameterDoubleWithDefault("hetF", 0.75);
	logfile->list("Will simulate father's genotypes with heterozygosity " + toString(hetFather) + ".");

	//read transition matrix parameters
	logfile->startIndent("Initializing transition matrix:");
	initialize(params.getParameterIntWithDefault("numStates", 10));
	float kappa = params.getParameterDoubleWithDefault("kappa", 0.0001);
	if(kappa <= 0.0)
		throw "kappa needs to be positive!";
	logfile->list("Will use kappa = " + toString(kappa));

	//initialize transition matrix
	logfile->listFlush("Initializing transition matrix ...");
	TSquareMatrixStorage Q;
	sites.setNumStates(numStates);
	sites.fillTransitionMatrix(Q, kappa, distance);
	logfile->write(" done!");
	logfile->conclude("Given the chosen kappa and distance, the transition probability is " + toString(1-Q(2,2)));
	logfile->endIndent();

	//initialize distortion model / transmission probabilities
	initializeSegregationProbabilities(params);
	segProbs->fillCumulative();

	//genotyping errors
	float error = params.getParameterDoubleWithDefault("error", 0.05);
	float oneMinusError = 1.0 - error;
	logfile->list("Will assume a per allele genotyping error of " + toString(error) + ".");

	//fill likelihood table
	TSquareMatrixStorage likelihood(3); //row=true, col=observed
	likelihood(0,0) = oneMinusError * oneMinusError;
	likelihood(0,1) = 2.0 * oneMinusError * error;
	likelihood(0,2) = error * error;
	likelihood(1,0) = oneMinusError * error;
	likelihood(1,1) = oneMinusError * oneMinusError + error * error;
	likelihood(1,2) = oneMinusError * error;
	likelihood(2,0) = error * error;
	likelihood(2,1) = 2.0 * oneMinusError * error;
	likelihood(2,2) = oneMinusError * oneMinusError;

	//get cumulative
	TSquareMatrixStorage likelihoodCumul;
	likelihoodCumul.fillAsCumulative(likelihood);

	//get Phred-scale
	TSquareMatrixStorage likelihoodPhred(3);
	for(int i=0; i<3; ++i){
		for(int j=0; j<3; ++j){
			likelihoodPhred(i,j) = -10.0 * log10(likelihood(i,j));
		}
	}

	//----------------------
	//open files for writing
	//----------------------
	logfile->startIndent("Opening files to write output to:");
	std::string outname = params.getParameterStringWithDefault("out", "segDistort_simulations");
	logfile->list("Will write output files with tag '" + outname + "'.");

	//distortions
	std::string distortionFilename = outname + "_trueDistortions.txt";
	logfile->listFlush("Opening file '" + distortionFilename + "' for true distortions ...");
	std::ofstream distortionOut(distortionFilename.c_str());
	if(!distortionOut)
		throw "Failed to open file '" + distortionFilename + "' for writing!";
	distortionOut << "site\tposition\tstate\tdistortion\n";
	logfile->done();

	//genotypes
	std::string genoFilename = outname + "_trueGenotypes.txt";
	logfile->listFlush("Opening file '" + genoFilename + "' for true genotypes ...");
	std::ofstream genoOut(genoFilename.c_str());
	if(!genoOut)
		throw "Failed to open file '" + genoFilename + "' for writing!";
	//write header
	genoOut << "site\tposition\tmother\tfather";
	for(int o=0; o<numOffspring; ++o)
		genoOut << "\toffspring" << o+1;
	genoOut << "\n";
	logfile->done();

	//VCF file
	std::string vcfFilename = outname + ".vcf.gz";
	logfile->listFlush("Opening vcf file '" + vcfFilename + "' ...");
	gz::ogzstream vcf(vcfFilename.c_str());
	if(!vcf)
		throw "Failed to open file '" + vcfFilename + "' for writing!";
	//write vcf header
	vcf << "##fileformat=VCFv4.2\n";
	vcf << "##source=SegDistort\n";
	vcf << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
	vcf << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n";
	vcf << "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled genotype likelihoods\">\n";
	vcf << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tmother\tfather";
	for(int o=0; o<numOffspring; ++o)
		vcf << "\toffspring" << o+1;
	vcf << "\n";
	logfile->done();
	logfile->endIndent();
	logfile->endIndent();

	//---------------------
	//loop over chromosomes
	//---------------------
	//prepare storage
	int* distortions = new int[numSites];
	short* mother = new short[numSites];
	short* father = new short[numSites];
	short** offspring = new short*[numSites];
	for(int l=0; l<numSites; ++l)
		offspring[l] = new short[numOffspring];

	//now loop
	for(int chr=0; chr<numChr; ++chr){
		logfile->startIndent("Simulating chromosome " + toString(chr+1) + ":");

		// 1) Simulate distortions
		//------------------------
		//get cumulative distributions
		logfile->startIndent("Simulating distortions:");
		TSquareMatrixStorage Qcumul;
		Qcumul.fillAsCumulative(Q);

		//and simulate distortions, always starting from zero
		logfile->listFlush("Simulating distortions ...");

		distortions[0] = 0;
		for(int i=1; i<numSites; ++i){
			distortions[i] = randomGenerator->pickOne(numStates, Qcumul.Row(distortions[i-1]));
		}
		logfile->write(" done!");

		//write distortions to file
		logfile->listFlush("Writing true distortions to '" + distortionFilename + "' ...");
		for(int i=0; i<numSites; ++i){
			distortionOut << i+1 << "\t" << i*distance << "\t" << distortions[i] << "\t" << distortionStrength[distortions[i]] << "\n";
		}
		logfile->write(" done!");
		logfile->endIndent();

		// 2) Simulate genotypes
		//------------------------
		//simulate parentals
		logfile->startIndent("Simulating genotypes:");

		logfile->listFlush("Simulating parental genotypes ...");

		for(int l=0; l<numSites; ++l){
			//mother
			if(randomGenerator->getRand() < hetMother)
				mother[l] = 1;
			else mother[l] = round(randomGenerator->getRand()) * 2;
			//father
			if(randomGenerator->getRand() < hetFather)
				father[l] = 1;
			else father[l] = round(randomGenerator->getRand()) * 2;
		}
		logfile->write(" done!");

		//now simulate offspring
		logfile->listFlush("Simulating genotypes of offspring ...");
		for(int l=0; l<numSites; ++l){
			if(randomGenerator->getRand() < 0.5){
				for(int o=0; o<numOffspring; ++o)
					offspring[l][o] = segProbs->sampleOffspringGenotype(mother[l], father[l], distortions[l], *randomGenerator);
			} else  {
				for(int o=0; o<numOffspring; ++o)
					offspring[l][o] = segProbs->sampleOffspringGenotypeMinus(mother[l], father[l], distortions[l], *randomGenerator);
			}
		}
		logfile->write(" done!");

		//writing true genotypes to a file
		logfile->listFlush("Writing true genotypes to '" + genoFilename + "' ...");
		for(int l=0; l<numSites; ++l){
			genoOut << l+1 << "\t" << l*distance << "\t" << mother[l] << "\t" << father[l];
			for(int o=0; o<numOffspring; ++o)
				genoOut << "\t" << offspring[l][o];
			genoOut << "\n";
		}
		logfile->done();
		logfile->endIndent();

		// 3) Simulate data and write VCF
		//-------------------------------
		//now simulate genotyping data and write to file
		logfile->listFlush("Simulating genetic data and writing it vcf file ...");
		for(int l=0; l<numSites; ++l){
			//write site info
			vcf << chr+1 << "\t" << distance * l + 1 << "\t.\tA\tC\t50\t.\t.\tGT:GQ:PL";

			//samples
			simulateGenotype(vcf, mother[l], likelihoodPhred, likelihoodCumul);
			simulateGenotype(vcf, father[l], likelihoodPhred, likelihoodCumul);
			for(int o=0; o<numOffspring; ++o)
				simulateGenotype(vcf, offspring[l][o], likelihoodPhred, likelihoodCumul);

			vcf << "\n";
		}
		logfile->done();

		//end of loop over chromosomes
		logfile->endIndent();
	}
	//--------------------------------

	//clean up
	distortionOut.close();
	genoOut.close();
	vcf.close();
	delete[] mother;
	delete[] father;
	for(int l=0; l<numSites; ++l)
		delete[] offspring[l];
	delete[] offspring;
	delete[] distortions;
}


void TSegDistort_core::simulateGenotype(gz::ogzstream & vcf, short & trueGenotype, TSquareMatrixStorage & likelihoodPhred, TSquareMatrixStorage & likelihoodCumul){
	int geno = randomGenerator->pickOne(3, likelihoodCumul.Row(trueGenotype));
	if(geno == 0) vcf << "\t0/0";
	else if(geno == 1) vcf << "\t0/1";
	else vcf << "\t1/1";
	vcf << ":30:" << likelihoodPhred(0,geno) << "," << likelihoodPhred(1,geno) << "," << likelihoodPhred(2,geno);
}












