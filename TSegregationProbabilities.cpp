/*
 * TSegregationProbabilities.cpp
 *
 *  Created on: Jun 15, 2018
 *      Author: wegmannd
 */

#include "TSegregationProbabilities.h"


//-----------------------------------------------------
//TSegregationProbabilities
//-----------------------------------------------------
TSegregationProbabilities::TSegregationProbabilities(int NumStates){
	initializeStorage(NumStates);
	cumulativeStorageInitialized = false;
	segProbCumul = NULL;
	segProbMinusCumul = NULL;
};

TSegregationProbabilities::TSegregationProbabilities(int NumStates, double* distortionStrengths, double Mu):TSegregationProbabilities(NumStates){
	fillSegregationProbabilities(distortionStrengths, Mu);
};

void TSegregationProbabilities::initializeStorage(int NumStates){
	numStates = NumStates;
	sizeOfEmissionStorage = 9*numStates;

	//create Storage
	segProb = new double*[sizeOfEmissionStorage];
	segProbMinus = new double*[sizeOfEmissionStorage];
	for(int i=0; i<sizeOfEmissionStorage; ++i){
		segProb[i] = new double[3];
		segProbMinus[i] = new double[3];
	}

	storageInitialized = true;
};

void TSegregationProbabilities::freeStorage(){
	if(storageInitialized){
		for(int i=0; i<sizeOfEmissionStorage; ++i){
			delete[] segProb[i];
			delete[] segProbMinus[i];
		}

		delete[] segProb;
		delete[] segProbMinus;
	}
	storageInitialized = false;

	//cumulative storage
	freeCumulativeStorage();
};

void TSegregationProbabilities::fillTransmissionMatrixMother(double** mother, double & distortionStrengh){
	//matrix[parent genotype][allele transmitted]
	mother[0][0] = 1.0;
	mother[1][0] = 0.5 + distortionStrengh;
	mother[2][0] = 0.0;

	for(int g=0; g<3; ++g)
		mother[g][1] = 1.0 - mother[g][0];

};

void TSegregationProbabilities::fillTransmissionMatrixFather(double** father, double & distortionStrengh){
	//same as mother
	fillTransmissionMatrixMother(father, distortionStrengh);
}

void TSegregationProbabilities::fillSegregationProbabilities(double* distortionStrength, double Mu){
	//fill for plus distortion an minus distortion seperately to be general
	fillSegregationProbabilities(segProb, distortionStrength, Mu);

	//and now minus
	double* distortionStrengthMinus = new double[numStates];
	for(int i=0; i<numStates; ++i)
		distortionStrengthMinus[i] = -distortionStrength[i];

	fillSegregationProbabilities(segProbMinus, distortionStrengthMinus, Mu);
	delete[] distortionStrengthMinus;
};

void TSegregationProbabilities::fillSegregationProbabilities(double** segregationProbabilities, double* distortionStrength, double Mu){
	//parental transmission matrices
	double** mother = new double*[3];
	double** father = new double*[3];
	for(int g=0; g<3; ++g){
		mother[g] = new double[2];
		father[g] = new double[2];
	}

	//tmp variables
	double tmpProb[3];
	double muHalf = Mu / 2.0;
	double oneMinusMu = 1.0 - Mu;


	//fill segregation probabilities for each state
	for(int i=0; i<numStates; ++i){

		//fill per parental matrices
		fillTransmissionMatrixMother(mother, distortionStrength[i]);
		fillTransmissionMatrixFather(father, distortionStrength[i]);

		//now do product for full matrix
		for(short m=0; m<3; ++m){
			for(short f=0; f<3; ++f){
				//get index
				int index = getIndex(m, f, i);

				//now fill product
				tmpProb[0] = mother[m][0] * father[f][0];
				tmpProb[1] = mother[m][0] * father[f][1] + mother[m][1] * father[f][0];
				tmpProb[2] = mother[m][1] * father[f][1];

				//add effect of mu
				//mu is a genotyping error term for offspring such that
				//P(g_observed = g_true) = 1-mu, P(g_obs!=g_true) = mu/2 since there are two possible errors
				segregationProbabilities[index][0] = oneMinusMu * tmpProb[0] + muHalf*tmpProb[1] + muHalf*tmpProb[2];
				segregationProbabilities[index][1] = oneMinusMu * tmpProb[1] + muHalf*tmpProb[0] + muHalf*tmpProb[2];
				segregationProbabilities[index][2] = oneMinusMu * tmpProb[2] + muHalf*tmpProb[0] + muHalf*tmpProb[1];
			}
		}

	}

	//clean up
	for(int g=0; g<3; ++g){
		delete[] mother[g];
		delete[] father[g];
	}
	delete[] mother;
	delete[] father;
};

void TSegregationProbabilities::initCumulativeStorage(){
	if(!cumulativeStorageInitialized){
		segProbCumul = new double*[sizeOfEmissionStorage];
		segProbMinusCumul = new double*[sizeOfEmissionStorage];
		for(int i=0; i<sizeOfEmissionStorage; ++i){
			segProbCumul[i] = new double[3];
			segProbMinusCumul[i] = new double[3];
		}
	}
	cumulativeStorageInitialized = true;
};

void TSegregationProbabilities::freeCumulativeStorage(){
	if(cumulativeStorageInitialized){
		for(int i=0; i<sizeOfEmissionStorage; ++i){
			delete[] segProbCumul[i];
			delete[] segProbMinusCumul[i];
		}

		delete[] segProbCumul;
		delete[] segProbMinusCumul;

	}
};

void TSegregationProbabilities::fillCumulative(){
	//create storage
	initCumulativeStorage();

	//fill cumulative
	for(int i=0; i<sizeOfEmissionStorage; ++i){
		segProbCumul[i][0] = segProb[i][0];
		segProbCumul[i][1] = segProb[i][0] + segProb[i][1];
		segProbCumul[i][2] = 1.0;

		segProbMinusCumul[i][0] = segProbMinus[i][0];
		segProbMinusCumul[i][1] = segProbMinus[i][0] + segProbMinus[i][1];
		segProbMinusCumul[i][2] = 1.0;
	}
};

int TSegregationProbabilities::sampleOffspringGenotype(short & mother, short & father, int & distortionIndex, TRandomGenerator & randomGenerator){
	int index = getIndex(mother, father, distortionIndex);
	return randomGenerator.pickOne(3, segProbCumul[index]);
};

int TSegregationProbabilities::sampleOffspringGenotypeMinus(short & mother, short & father, int & distortionIndex, TRandomGenerator & randomGenerator){
	int index = getIndex(mother, father, distortionIndex);
	return randomGenerator.pickOne(3, segProbMinusCumul[index]);
};

void TSegregationProbabilities::printSegProbs(){
	//print segregation matrices
	//header
	std::cout << "mother\tfather\toff_0\toff_1\toff_2\toff_0_minus\toff_1_minus\toff_2_minus\n";
	for(int i=0; i<numStates; ++i){
		for(short m=0; m<3; ++m){
			for(short f=0; f<3; ++f){
				//get index
				int index = getIndex(m, f, i);

				//print
				std::cout << m << "\t" << f << "\t" << segProb[index][0] << "\t" << segProb[index][1] << "\t" << segProb[index][2];
				std::cout << "\t" << segProbMinus[index][0] << "\t" << segProbMinus[index][1] << "\t" << segProbMinus[index][2] << "\n";
			}
		}

	}
};

//----------------------------------------------------------------
//TSegregationProbabilitiesBackcross
//----------------------------------------------------------------
TSegregationProbabilitiesBackcross::TSegregationProbabilitiesBackcross(int NumStates, double* distortionStrengths, double Mu):TSegregationProbabilities(NumStates){
	fillSegregationProbabilities(distortionStrengths, Mu);
};

void TSegregationProbabilitiesBackcross::fillTransmissionMatrixFather(double** father, double & distortionStrengh){
	//matrix[parent genotype][allele transmitted]
	father[0][0] = 1.0;
	father[1][0] = 0.5;
	father[2][0] = 0.0;

	for(int g=0; g<3; ++g)
		father[g][1] = 1.0 - father[g][0];

};

void TSegregationProbabilitiesBackcross::fillSegregationProbabilities_OLD(double* distortionStrength, double Mu){
	//fill segregation probabilities
	double muHalf = Mu / 2.0;
	int j;
	for(int i=0; i<numStates; ++i){
		j = i*9;
		//g_m = g_f = 0
		segProb[j][0] = 1.0 - Mu;
		segProb[j][1] = muHalf;
		segProb[j][2] = muHalf;

		//g_m=0, g_f=1
		segProb[j+1][0] = 0.5 * (1.0 - Mu) + 0.5 * muHalf;
		segProb[j+1][1] = 0.5 * (1.0 - Mu) + 0.5 * muHalf;
		segProb[j+1][2] = muHalf;

		//g_m=0, g_f=2
		segProb[j+2][0] = muHalf;
		segProb[j+2][1] = 1.0 - Mu;
		segProb[j+2][2] = muHalf;

		//g_m=1, g_f=0
		segProb[j+3][0] = (0.5 + distortionStrength[i]) * (1.0 - Mu) + (0.5 - distortionStrength[i]) * muHalf;
		segProb[j+3][1] = (0.5 - distortionStrength[i]) * (1.0 - Mu) + (0.5 + distortionStrength[i]) * muHalf;
		segProb[j+3][2] = muHalf;

		//g_m=1, g_f=1
		segProb[j+4][0] = (1.0 + 2.0*distortionStrength[i]) / 4.0 * (1.0 - Mu) + (0.5 + (1.0 - 2.0*distortionStrength[i]) / 4.0) * muHalf;
		segProb[j+4][1] = (2.0 - Mu) / 4.0;
		segProb[j+4][2] = (1.0 - 2.0*distortionStrength[i]) / 4.0 * (1.0 - Mu) + (0.5 + (1.0 + 2.0*distortionStrength[i]) / 4.0) * muHalf;

		//g_m=1, g_f=2
		segProb[j+5][0] = muHalf;
		segProb[j+5][1] = (0.5 + distortionStrength[i]) * (1.0 - Mu) + (0.5 - distortionStrength[i]) * muHalf;
		segProb[j+5][2] = (0.5 - distortionStrength[i]) * (1.0 - Mu) + (0.5 + distortionStrength[i]) * muHalf;

		//g_m=2, g_f=0
		segProb[j+6][0] = muHalf;
		segProb[j+6][1] = 1.0 - Mu;
		segProb[j+6][2] = muHalf;

		//g_m=2, g_f=1
		segProb[j+7][0] = muHalf;
		segProb[j+7][1] = 0.5 * (1.0 - Mu) + 0.5 * muHalf;
		segProb[j+7][2] = 0.5 * (1.0 - Mu) + 0.5 * muHalf;

		 //g_m = g_f = 2
		segProb[j+8][0] = muHalf;
		segProb[j+8][1] = muHalf;
		segProb[j+8][2] = 1.0 - Mu;
	}

	//now that for minus pi_l
	for(int i=0; i<sizeOfEmissionStorage; ++i){
		segProbMinus[i][0] = segProb[i][0];
		segProbMinus[i][1] = segProb[i][1];
		segProbMinus[i][2] = segProb[i][2];
	}
};



