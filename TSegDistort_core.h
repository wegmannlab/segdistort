/*
 * TSegDistort_core.h
 *
 *  Created on: Apr 13, 2017
 *      Author: phaentu
 */

#ifndef TSEGDISTORT_CORE_H_
#define TSEGDISTORT_CORE_H_

#include "TLog.h"
#include "TParameters.h"
#include "TRandomGenerator.h"
#include "TVcfFile.h"
#include "TSegregationProbabilities.h"

#include <algorithm>
#include "gzstream.h"
#include "TMatrix.h"
#include <sys/time.h>


//-----------------------------------------------------
//TDistanceGroup
//-----------------------------------------------------
struct TDistanceGroup{
	int min;
	int maxPlusOne;
	double distance;
};

//-----------------------------------------------------
//TSites
//-----------------------------------------------------
class TSites{
private:
	bool initialized;
	bool Q_initialized;
	bool* groupHasSites;
	int* groupMembership;
	TDistanceGroup* distanceGroups;

	//for estimation
	double*** gamma_ij;
	bool gamma_ij_initalized;

	double estimateKappaQ2();

public:
	long numSites;
	int numStates;
	int numDistanceGroups;
	TSquareMatrixStorage* Q;

	bool* siteIsBeginningOfChromosome;

	TSites(){
		numSites = 0;
		numDistanceGroups = 0;
		distanceGroups = NULL;
		groupHasSites = NULL;
		groupMembership = NULL;
		siteIsBeginningOfChromosome = NULL;
		initialized = false;
		Q = NULL;
		Q_initialized = false;
		gamma_ij = NULL;
		gamma_ij_initalized = false;
		numStates = -1;

	};
	~TSites(){
		if(initialized){
			delete[] distanceGroups;
			delete[] groupHasSites;
			delete[] groupMembership;
			delete[] siteIsBeginningOfChromosome;
		}
		if(Q_initialized){
			delete[] Q;
		}
		if(gamma_ij_initalized){
			for(int d=0; d<numDistanceGroups; ++d){
				for(int i=0; i<numStates; ++i)
					delete[] gamma_ij[d][i];
				delete[] gamma_ij[d];
			}
			delete[] gamma_ij;
		}
	};
	void setNumStates(int NumStates){numStates = NumStates;};
	void fill(std::vector<long> & distances, long & maxDist);
	void fillTransitionMatrix(TSquareMatrixStorage & matrix, const double & kappa, const double distance);
	void fillAllTransitionProbabilityMatrices(const double & kappa);
	double& transProb(const long & locus, const int & from, const int & to);
	void setGamma_ij_zero();
	void addToGamma_ij(double val, const long & locus, const int & from, const int & to);
	void updateInitialFrequency(double* aj);
	double getNewKappaEstimate(double curKappa, int NumSwitches);
	void calcKappQ2Surface(std::string filename);
};

//-----------------------------------------------------
//TSegDistort_core
//-----------------------------------------------------
class TSegDistort_core{
private:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	TVcfFileSingleLine vcfFile;
	int numStates;
	TSites sites;

	//store details on sites
	std::vector<long> distances;
	std::vector<long>::iterator distIt;
	std::vector<std::string> chromosomes;
	std::vector<long> initialPos;
	std::vector<long>::iterator initialPosIt;
	std::vector<std::string>::iterator chrIt;

	//emission probabilities at one site are stored as a single array with index i = (3*g_mother + g_father) + state * 9
	double** emissionProbabilities;
	bool emissionProbabilitiesInitialized;

	//segregation probabilities are stored as a 2D array[i,j] with index i = (3*g_mother + g_father) + state * 9 (like emission probabilities) and j=genotype of offspring
	double* distortionStrength;
	bool initialized;
	TSegregationProbabilities* segProbs;
	bool segProbInitialized;

	//HMM variables
	double** alpha;
	double* beta;
	double* betaNext;
	double* gamma_i;
	double** gamma_ij;
	double* initialFreq_aj;
	bool hmmVariablesInitialized;

	void clear();
	void initialize(int NumStates);
	void createHMMstorage();
	void printProgressReadData(long lines, long & snps, int chromosomes, struct timeval & start);
	void readData(std::string filename, std::string motherName, std::string fatherName, long maxNumSites, bool requireParentsHaveData, bool requireOneParentHasData, double missingness);
	void writeEmissionprobabilities(std::string & outname);
	void initializeHMM();
	double runForward();
	void runBackwardAndEstimate(double curKappa, int maxNumIterationsKappa, int iteration, bool estimateKappa);
	void runBackwardAndPrintPosteriorProbabilities(std::string filename);
	void initializeSegregationProbabilities(TParameters & params);

public:

	TSegDistort_core(TLog* Logfile, TRandomGenerator* RandomGenerator);
	~TSegDistort_core(){
		clear();
	};

	void infer(TParameters & params);
	void simulateData(TParameters & params);
	void simulateGenotype(gz::ogzstream & vcf, short & trueGenotype, TSquareMatrixStorage & likelihoodPhred, TSquareMatrixStorage & likelihoodCumul);
};


#endif /* TSEGDISTORT_CORE_H_ */
